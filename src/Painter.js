/**
 * copyright 2018- commenthol
 * license MIT
 */

import { h, Component } from 'preact'
import { Canvas, TOOLS } from './Canvas'
import { IconFa } from './IconFa'
import { addListener } from './addListener'
import './Painter.css'

const colors = ['#00a', '#000', '#f33', '#090', '#f93', '#ff3', '#3ff', '#f3f']

export class Painter extends Component {
  /**
   * @constructor
   * @param {Number} props.width - in pixel
   * @param {Number} props.height - in pixel
   * @param {Number} props.idx - whiteboard index in page
   * @param {EventTarget} [props.emitter] - just receiving commands; listens on `change`
   * @param {Function} [props.onDraw] - onDraw action
   */
  constructor (props) {
    super(props)
    const { isRx } = props

    this.onRxEvent = this.onRxEvent.bind(this)
    this.state = {
      isRx,
      tool: TOOLS.PENCIL,
      showColors: false,
      switchOnColors: true,
      style: {
        strokeStyle: colors[0],
        lineJoin: 'round',
        lineWidth: 2
      }
    }
  }

  componentDidMount () {
    const { emitter } = this.props
    if (emitter) {
      this.listener = addListener(emitter, this.onRxEvent)
    }
  }

  componentWillUnmount () {
    this.listener && this.listener.remove()
  }

  /**
   * @param {CustomEvent|Object} ev - if CustomEvent than ev.detail contains payload
   */
  onRxEvent (payload) {
    const { idx } = this.props
    const { isRx } = payload
    if (isRx !== undefined && isRx !== this.state.isRx) { // component is in rx mode
      this.setState({ isRx })
    }

    if (payload.idx === idx) {
      const { cmd, curr, prev, style, erase, tool } = payload
      if (cmd) { // drawing...
        this.canvas.draw(cmd, curr, prev)
      } else if (style) { // set style
        this.setState({ style })
      } else if (erase) { // erase canvas
        this.canvas.erase()
      } else if (tool) { // set tool
        this.setTool(tool)
      }
    }
  }

  onDraw (...args) {
    const { isRx } = this.state
    const { onDraw } = this.props
    !isRx && onDraw && onDraw(...args)
  }

  setTool (tool) {
    const { idx } = this.props
    this.onDraw({ idx, tool })
    this.setState({ tool })
  }

  renderColor (backgroundColor, key) {
    const { idx } = this.props
    const onClick = () => {
      const { style } = this.state
      style.strokeStyle = backgroundColor
      this.setTool(TOOLS.PENCIL)
      this.onDraw({ idx, style })
      this.setState({ style, showColors: false })
    }
    const props = {
      key,
      onClick,
      className: 'toolbar__color',
      style: { backgroundColor }
    }
    return (<span {...props} />)
  }

  renderColorSelection () {
    const { showColors } = this.state
    const className = `toolbar__colors ${showColors ? '' : 'toolbar__colors--hide'}`
    return (
      <div className={className}>
        <div className='toolbar__colors_hor'>
          {colors.map((color, i) => this.renderColor(color, i))}
        </div>
      </div>
    )
  }

  renderToolbar () {
    const { tool, style, showColors, switchOnColors } = this.state
    const { strokeStyle: color } = style
    const pencilStyle = (tool !== TOOLS.PENCIL) ? {} : { color }
    return (
      <div className='toolbar'>
        {this.renderColorSelection()}
        <div className={`toolbar__icon ${tool !== TOOLS.PENCIL ? 'toolbar__icon--inactive' : ''}`}
          onClick={() => {
            this.setState({ showColors: switchOnColors ? !showColors : showColors, switchOnColors: true })
            this.setTool(TOOLS.PENCIL)
          }}>
          <IconFa icon="palette" style={pencilStyle} />
        </div>
        <div className={`toolbar__icon ${tool !== TOOLS.ERASER ? 'toolbar__icon--inactive' : ''}`}
          onClick={() => {
            this.setState({ showColors: false, switchOnColors: false })
            this.setTool(TOOLS.ERASER)
          }}>
          <IconFa icon="eraser" />
        </div>
        <div className='toolbar__icon'
          onClick={() => { this.canvas.erase() }}>
          <IconFa icon="broom" />
        </div>
      </div>
    )
  }

  render () {
    const { isRx, style, tool } = this.state
    const { idx, width, height, onDraw } = this.props
    const props = {
      idx,
      width,
      height,
      onDraw,
      isRx,
      style,
      tool,
      ref: (canvas) => { this.canvas = canvas }
    }
    return (
      <div className={`painter painter--${tool}`}>
        {!isRx && this.renderToolbar()}
        <Canvas {...props}></Canvas>
      </div>
    )
  }
}
