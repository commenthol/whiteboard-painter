/**
 * copyright 2018- commenthol
 * license MIT
 */

import { h, render } from 'preact'
import { Painter } from './Painter'
import { $, jqall } from './jq'
import './x-whiteboard.css'

/**
 * @param {EventTarget} [props.emitter] - receives remote commands
 * @param {Function} [props.onDraw] - draw action
 */
export function xWhiteboards (props) {
  jqall('x-whiteboard').each((el, idx) => {
    const $el = $(el)
    const elProps = $el.attr()
    elProps.isRx = (elProps.rx === '')
    const _props = Object.assign({}, props, elProps, {
      idx,
      width: el.clientWidth,
      height: el.clientHeight
    })
    render(<Painter {..._props} />, el)
  })
}
