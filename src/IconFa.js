/**
 * copyright 2018- commenthol
 * license MIT
 */

import { h, Component } from 'preact'

import '@fortawesome/fontawesome-free/css/solid.css'
import '@fortawesome/fontawesome-free/css/regular.css'
import '@fortawesome/fontawesome-free/css/fontawesome.css'

/**
 * fontawesome icons
 * @see https://fontawesome.com/icons?d=gallery&s=brands,light,regular,solid&m=free
 * properties
 * @param {string} icon - icon name e.g. 'address-book'
 * @param {string} [family='solid'] - font family (solid|regular)
 */
export class IconFa extends Component {
  render () {
    const { icon, family = 'solid', className, ...props } = this.props
    props.className = `fa${family[0]} fa-${icon} ${className || ''}`
    return (
      <i {...props}/>
    )
  }
}
