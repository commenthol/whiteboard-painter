/**
 * @copyright 2018- commenthol
 * @license MIT
 */

import { xWhiteboards } from './x-whiteboard'
import { xNotes } from './x-note'
export { xWhiteboards, xNotes }
export { Painter } from './Painter'
export { Note } from './Note'
export { jq, jqall, isElement } from './jq'

(function () {
  if (typeof window !== 'undefined') {
    window.xWhiteboards = (...args) => xWhiteboards(...args)
    window.xNotes = (...args) => xNotes(...args)
  }
})()
