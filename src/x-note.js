/**
 * copyright 2018- commenthol
 * license MIT
 */

import { h, render } from 'preact'
import { Note } from './Note'
import { $, jqall } from './jq'
import './x-note.css'

/**
 * @param {EventTarget} [props.emitter] - receives remote commands
 */
export function xNotes (props) {
  jqall('x-note').each((el, idx) => {
    const $el = $(el)
    const elProps = $el.attr()
    elProps.isRx = (elProps.rx === '')
    elProps.isOpen = (elProps.open === '')
    elProps.right = (elProps.right === '')
    elProps.top = (elProps.top === '')
    const _props = Object.assign({}, props, elProps, {
      idx,
      __html: $el.html()
    })
    $el.empty()
    render(<Note {..._props} />, el)
    $el.addClass('active')
  })
}
