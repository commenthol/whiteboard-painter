/**
 * copyright 2019- commenthol
 * license MIT
 */

/**
 * @typedef {Object} Remover
 * @property {Function} remove
 */
/**
 * @param {EventTarget} emitter
 * @param {Function} fn - function which is added to emitter
 * @param {String} [type=change]
 * @returns {Remover}
 */
export const addListener = (emitter, fn, type = 'change') => {
  const _fn = (ev) => fn(ev.detail || ev)
  emitter.addEventListener(type, _fn)
  return {
    remove: () => emitter.removeEventListener(type, _fn)
  }
}
