/**
 * copyright 2018- commenthol
 * license MIT
 */

import { h, Component } from 'preact'
import { jq } from './jq'

export const TOOLS = {
  PENCIL: 'pencil',
  ERASER: 'eraser'
}
const DEFAULT_STYLE = {
  strokeStyle: '#00a',
  lineJoin: 'round',
  lineWidth: 2
}
const CMD = {
  DOWN: 'down',
  MOVE: 'move'
}

const isLinux = /linux/i.test(navigator.platform)
const filterEvent = (ev) => isLinux && ev.pointerType === 'touch'
const stopPropagation = (ev) => {
  ev.preventDefault()
  ev.stopPropagation()
  return false
}

export class Canvas extends Component {
  /**
   * @constructor
   * @param {Number} props.width - in pixel
   * @param {Number} props.height - in pixel
   * @param {Number} props.idx - whiteboard index in page
   * @param {Object} [props.style] - canvas style for drawing
   * @param {String} [props.tool] - pencil|eraser
   * @param {Function} [props.ref] - return DOM node ref
   * @param {Boolean} [props.isRx=false] - `true` if receiving commands
   * @param {Function} [props.onDraw] - onDraw action
   */
  constructor (props) {
    super(props)
    const { idx, width, height, onDraw, isRx, style } = props

    const ref = (canvas) => {
      this.canvas = canvas
      this.context = canvas.getContext('2d')
      this.setStyle(this.style)
    }

    this.style = Object.assign({}, DEFAULT_STYLE, style)

    this.state = {
      tool: TOOLS.PENCIL,
      width,
      height,
      idx,
      ref
    }

    this.onDraw = (...args) => {
      !isRx && onDraw && onDraw(...args)
    }

    // window$ uses 'touch' event for start but 'pen' for move
    Object.assign(this.state, {
      onPointerDown: (ev) => {
        if (this.props.isRx || filterEvent(ev)) return
        // prevent stuff from scrolling
        jq(document.body).attr('style', 'overflow: hidden;')
        this.isDrawing = true
        this.draw(CMD.DOWN, {
          x: ev.pageX - this.canvas.offsetLeft,
          y: ev.pageY - this.canvas.offsetTop
        })
        return stopPropagation(ev)
      },
      onPointerMove: (ev) => {
        if (this.props.isRx || !ev.pointerType || !this.isDrawing || filterEvent(ev)) return
        this.draw(CMD.MOVE, {
          x: ev.pageX - this.canvas.offsetLeft,
          y: ev.pageY - this.canvas.offsetTop
        })
        return stopPropagation(ev)
      },
      onPointerUp: (ev) => {
        // TODO - pointerup event not fired in win-chrome@73
        // let it scroll again
        jq(document.body).attr('style', '')
        this.isDrawing = false
        this.prev = null
        return stopPropagation(ev)
      },
      onPointerLeave: (ev) => {
        this.isDrawing = false
        this.prev = null
        return stopPropagation(ev)
      },
      onContextMenu: (ev) => {
        // surpress context menu
        return stopPropagation(ev)
      }
    })
  }

  componentDidUpdate () {
    this.context = this.canvas.getContext('2d')
    this.setStyle(this.style)
  }

  componentWillReceiveProps (next) {
    Object.assign(this.style, next.style)
  }

  setStyle (style) {
    Object.entries(style).forEach(([k, v]) => {
      this.context[k] = v
    })
    const { idx } = this.props
    this.onDraw({ idx, style })
  }

  erase () {
    const { width, height, idx } = this.props
    this.context.clearRect(0, 0, width, height)
    this.onDraw({ idx, erase: true })
  }

  draw (cmd, curr, _prev) {
    const { context, prev } = this
    const { idx, isRx, tool } = this.props
    _prev = _prev || (cmd === CMD.DOWN ? curr : prev || curr)
    if ((isRx || this.isDrawing) && !isPalmDetect(_prev, curr, 200)) {
      this.onDraw({ idx, cmd, curr, prev: _prev })
      context.beginPath()
      context.moveTo(_prev.x, _prev.y)
      if (tool === TOOLS.ERASER) {
        context.clearRect(curr.x, curr.y, 20, 20)
      } else {
        context.lineTo(curr.x, curr.y)
        context.closePath()
        context.stroke()
      }
      this.prev = curr
    }
  }

  render () {
    return (<canvas {...this.state} />)
  }
}

function isPalmDetect (prev, curr, threshold) {
  const dx = prev.x - curr.x
  const dy = prev.y - curr.y
  const dist = Math.sqrt(Math.pow(dx, 2), Math.pow(dy, 2))
  return dist > threshold
}

export default Canvas
