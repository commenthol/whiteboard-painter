/**
 * copyright 2019- commenthol
 * license MIT
 */

import { h, Component } from 'preact'
import { IconFa } from './IconFa'
import { addListener } from './addListener'
import './Note.css'

const classnames = cn => Object.entries(cn)
  .map(([k, v]) => v && k).filter(cn => cn).join(' ')

export class Note extends Component {
  /**
   * @constructor
   * @param {EventTarget} [props.emitter] - just receiving commands; listens on `change`
   * @param {Boolean} [props.isRx] - is in rx mode, means is not displayed
   * @param {Boolean} [props.isOpen] - is open by default
   * @param {Boolean} [props.right] - right align
   * @param {Boolean} [props.top] - show on top
   * @param {Boolean} [props.width] - width of note
   * @param {Boolean} [props.height] - height of note
   * @param {Boolean} [props.opacity] - opacity
   */
  constructor (props) {
    super(props)
    const { isRx, isOpen } = props

    this.onRxEvent = this.onRxEvent.bind(this)
    this.state = {
      isRx,
      isOpen
    }
  }

  componentDidMount () {
    const { emitter } = this.props
    if (emitter) {
      this.listener = addListener(emitter, this.onRxEvent)
    }
  }

  componentWillUnmount () {
    this.listener && this.listener.remove()
  }

  /**
   * @param {CustomEvent|Object} ev - if CustomEvent than ev.detail contains payload
   */
  onRxEvent (payload) {
    const { isRx } = payload
    if (isRx !== undefined && isRx !== this.state.isRx) { // component is in rx mode
      this.setState({ isRx })
    }
  }

  render () {
    const { isRx, isOpen } = this.state
    if (isRx) return null
    const { __html, right, top, opacity, width, height } = this.props
    const toggle = () => this.setState({ isOpen: !isOpen })
    return (
      <div className={classnames({ note: 1, right, open })}>
        <div className='note__toggle' onClick={toggle}><IconFa icon='comment-alt' family='regular' /></div>
        {isOpen
          ? <div className={classnames({ 'note__content': 1, right, top })}
            style={{ opacity, width, height }}
            onClick={toggle}
            dangerouslySetInnerHTML={{ __html }} />
          : null
        }
      </div>
    )
  }
}
