<a name="README"></a>

# whiteboard-painter

Whiteboard painting canvas for *pen* based drawings on touchscreens in the browser.

Comes with the ability to add notes.

![screenshot](https://gitlab.com/commenthol/whiteboard-painter/raw/master/docs/screenshot.png)

Components are used in [scholae-dicimus](https://gitlab.com/commenthol/scholae-dicimus/) project.

## browser oddities

For Chrome on Windows 10 you'll need to disable "Direct Manipulation Stylus".
Open Chrome and enter `chrome://flags/#direct-manipulation-stylus` to disable flag.
Otherwise Stylus actions interfere with scroll events which does not allow painting.

## usage

```html
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="dist/index.css">
</head>
<body>
  <!-- Whiteboard for writing -->
  <x-whiteboard style="height: 50vh;"></x-whiteboard>

  <!-- Add some notes -->
  <x-note>
    a note text...
  </x-note>

  <script src="dist/index.js"></script>
  <script>
  // initialize components
  window.xWhiteboards()
  window.xNotes()
  </script>
</body>
</html>
```

## tests

```
npm run dev
```

Then open http://localhost:8080/

## license

[MIT licensed](./LICENSE)

Bundled fonts are from Font Awesome Free 5.x.x by @fontawesome - https://fontawesome.com
License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License)
